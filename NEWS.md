# 6.1.0

Unreleased.

# 6.0.0

Released on the 8th June 2019.

## Overview

Major changes include:

* OME-TIFF support for sub-resolutions (not yet exposed at the UI
  level).
* Removal of dependencies on Boost libraries (Boost header-only
  libraries are still required).

## Platform support

* C++14 is now the minimum required language version, with C++17 being
  used optionally when available (!51).
* Microsoft Visual Studio 2017 and 2019 are now both supported (!51).
* LLVM 6 is now supported.
* GCC 9 is now supported in C++14 mode; C++17 is currently unsupported
  since this release changed the type of
  `std::filesystem::file_time_type` returned by
  `std::filesystem::last_write_time()`, which requires non-portable
  conversion to `std::chrono::system_clock::time_point`.  This will be
  worked around in a future release, or with C++20 which will
  introduce portable time point conversion functions.

## Source changes

* Added support for sub-resolution images (not yet exposed at the UI
  level) (!43).
  * Add support for rendering RGB as well as greyscale.
  * Use QOpenGLWidget in place of custom widget.
  * Simplify OpenGL versioned classes.
* Use 4D pixel buffer (!45).
* Remove shared library, building a single executable (!47).
* Filesystem implementation is selectable using same strategy as
  ome-common-cpp (!49, !50).
* Remove Xerces and Xalan support (!52).
* Set GLM_ENABLE_EXPERIMENTAL for glm/gtx/rotate_vector.
* MPark.Variant was updated to the latest version.

## Infrastructure changes

* Continuous integration testing is performed on GitLab, testing on
  FreeBSD, Linux, MacOS X and Windows platforms (!44, !51, !54, !55,
  !56).
* Documentation link updates for GitLab migration (!48).

# 5.4.3

Released on the 1st December 2017.

* Drop embedded copies of CMake `FindBoost`, `FindXalanC` and
  `FindXercesC`.

# 5.4.2

Released on the 12th June 2017.

* Minor build improvements.

# 5.4.1

Released on the 29th May 2017.

* Minor build improvements.
* Updated README.

# 5.4.0

Released on the 10th February 2017.

* C++11 is now the minimum required language version, with C++14 being
  used when available.
  * Enabled the use of a restricted number of C++11 features,
    including `enum class`, `nullptr`, initializer lists, range-based for
    loops and type traits.
  * Enabled the use of C++11 syntax changes including `<::` not being
    interpreted as a trigraph and `>>` being used to close nested
    templates instead of `> >`.
  * Additional C++11 features will be enabled in subsequent releases.
* Google Test (gtest) is no longer built separately in each source
  component; the latest gtest release now allows use as a conventional
  library.

# 5.3.2

Released on the 12th October 2016.

* Boost 1.54 is the minimum supported version
* Add support for Boost 1.62

# 5.3.1

Released on the 19th August 2016

* Document all namespaces in API reference.

# 5.3.0

Released on the 29th July 2016.

* Support the 2016-06 data model.
* Upgraded the OpenGL requirement from 2.0 to 3.3 core; vertex and
  fragment shaders now use GLSL v330.
* Corrected Windows path handling.
* Removed VS2012 workarounds.
* Improved CMake configuration export.
* Added BSD licence text.
